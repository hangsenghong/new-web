<?php
include('connect.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" defer></script>
    <title>list account </title>
</head>
<body>
    <div class="container">
        <h2 style="text-align: center">LIST ACCOUNTS OF USER</h2>
        <br><br>
        <button class="btn btn-primary sm-2"><a href="create_user.php" class="text-light">Add account</a> </button>
        <br><br>
        <br>
        <table class="table table-bordered table-striped table-responsive-md" id="new_tbl">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th style="width: 20%;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = "SELECT * FROM `account`";
                $result = mysqli_query($conn, "SELECT * FROM `account`");
                if ($result) {
                    $row = mysqli_fetch_array($result);
                    // echo $row['name'];
                    // die(var_dump($row));
                    while ($row = mysqli_fetch_array($result)) {
                        $id = $row['id'];
                        $name = $row['name'];
                        $age = $row['age'];
                        $gender = $row['gender'];
                        echo '<tr>
                        <td>' . $id . '</td>
                        <td>' . $name . '</td>
                        <td>' . $age . '</td>
                        <td>' . $gender . '</td>
                        <td>
                            <button type="submit" class="btn btn-primary "><a href="update.php?updateid=' . $id . '" class="text-light">update</a> </button>
                            <button type="submit" class="btn btn-danger " id = "delete"class="text-light"><a href="delete.php?deleteid=' . $id . '"> delete</a></button>
                        </td>
                            
                        </tr>';
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <script>
        $(document).ready(function(){
            $('#new_tbl').DataTable();
        });
    </script>
</body>
</html>